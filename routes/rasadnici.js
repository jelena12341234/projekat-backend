const express = require('express');
const router = express.Router();



const config = require('../config/database')

const Rasadnik = require('../models/rasadnik');


router.post('/dodajRasadnik', (req,res,next)=>{

    const ras = [{
        naziv: req.body.naziv,
        username:req.body.username,
        mesto:req.body.mesto,
        brojSadnica:req.body.brojSadnica,
        brojSlMesta:req.body.brojSlMesta,
        kolVode:req.body.kolVode,
        temperatura:req.body.temperatura,
        sirina:req.body.sirina,
        visina:req.body.visina
    }];

    Rasadnik.insertMany(ras,err=>{
        if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});        
    });

});

router.post('/povecajTemp',(req,res,next)=>{
    const name = req.body.naziv;
    const temp = req.body.temperatura + 1 ;
    
    Rasadnik.changeTemp(name,temp,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });
});

router.post('/changeNumSadnicas',(req,res,next)=>{
    const name = req.body.naziv;
    const br1 = req.body.brojSadnica ;
    const br2 = req.body.brojSlMesta ;

    console.log("/changeNumSadnicas"+name+","+br1+","+br2);
      
    Rasadnik.changeNumSadnica(name,br1,br2,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });
});




router.post('/povecajVodu',(req,res,next)=>{
        const name = req.body.naziv;
        const water = req.body.kolVode + 1 ;
        Rasadnik.changeWater(name,water,err=>{
            if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});  
        });
});

/*
router.post('/dodajRas',(req,res,next)=>{
    const naz=req.body.naziv;
    const mes=req.body.mesto;
    const brSlobodnih = req.body.brojSlMesta;
    const korime = req.body.username;

    const noviRas = {
      "naziv":naz,
      "mesto":mes,
      "username":korime,
      "brojSadnica":0,
      "brojSlMesta":brSlobodnih,
      "kolVode":200,
      "temperatura":18
    };

    Rasadnik.inser


});

*/


router.post('/smanjiTemp',(req,res,next)=>{
    const name = req.body.naziv;
    const temp = req.body.temperatura - 1 ;
    
    Rasadnik.changeTemp(name,temp,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });

});

router.post('/smanjiVodu',(req,res,next)=>{
    const name = req.body.naziv;
    const water = req.body.kolVode - 1 ;
    Rasadnik.changeWater(name,water,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });


});



router.post('/vratiRasadnike', (req,res,next)=>{
    const korime = req.body.username;
    console.log("usli smo u /vratiRas sa usernamom : "+korime);
    
   Rasadnik.showRas(korime,(err,data)=>{
    if(err) res.json({success: false, msg: "error"});
    else res.json(data);
   });
});



module.exports = router;
