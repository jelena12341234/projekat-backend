const express = require('express');
const router = express.Router();


const config = require('../config/database');



const Sadnica = require('../models/sadnica');

router.post('/dodajSadnicu', (req,res,next)=>{

    const ras = [{
        nazivRasadnika: req.body.nazivRasadnika,
        username:req.body.username,
        mesto:req.body.mesto,
        naziv:req.body.naziv,
        proizvodjac:req.body.proizvodjac,
        progress:req.body.progress,
        row: req.body.row,
        col:req.body.col,
        izvadjena:false
    }];

    console.log(ras);

   Sadnica.insertMany(ras,err=>{
        if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});        
    });

});


router.post('/izvadi',(req,res,next)=>{
    const name = req.body.naziv;
    const korIme= req.body.username;
    const nazivrs = req.body.nazivRasadnika;
    const mesto = req.body.mesto;
    const kol = req.body.col;
    const red = req.body.row;

    console.log("/izvadi "+name+","+korIme+","+kol+","+nazivrs+","+mesto +","+kol+","+red);
    //azivrs, korIme,name, kol,red,place,

    Sadnica.izvadi(nazivrs, korIme,name, kol,red,mesto,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });
});
/**
 *     nazivRasadnika:string,
    username:string,
    mesto:string,
    naziv:string,
    proizvodjac:string,
    progress:number,
    row:number,
    col:number
 * 
 * 
 */
router.post('/obrisi',(req,res,next)=>{
    const name = req.body.naziv;
    const korIme= req.body.username;
    const nazivrs = req.body.nazivRasadnika;
    const mesto = req.body.mesto;
    const kol = req.body.col;
    const red = req.body.row;

    console.log("/obrisi"+name+","+korIme+","+kol+","+nazivrs);
    //azivrs, korIme,name, kol,red,place,
    Sadnica.obrisi(nazivrs, korIme,name, kol,red,mesto,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });
});

//korIme, place, nazivrs, nazivSad


router.post('/vratiSadnice', (req,res,next)=>{
    const korime = req.body.username;
    const nazivRas = req.body.nazivRasadnika;
    const place = req.body.mesto;
    const row = req.body.row;

   Sadnica.showSad(korime,place,nazivRas,row,(err,data)=>{
    if(err) res.json({success: false, msg: "error"});
    else res.json(data);
   });
});

router.post('/sadnicaInfo',(req,res,next)=>{
    const korime = req.body.username;
    const nazivRas = req.body.nazivRasadnika;
    const place = req.body.mesto;
    const proiz = req.body.proizvodjac;
    const nzv = req.body.naziv;
    const red = req.body.row;
    const kol = req.body.col

    //korIme, nzv, proiz, place, nazivrs
   Sadnica.showSadInfo(korime,nzv,proiz,place,nazivRas,red,kol,(err,data)=>{
    if(err) res.json({success: false, msg: "error"});
    else res.json(data);
   });

});

router.get('/updateDB',(req,res,next)=>{
    console.log("usli smo u sadnice DB");

    //"__v" : 0
    Sadnica.find().toArray().then(
        items=>{
            items.forEach(console.log)
        }
    ).catch(err => console.error(`Failed to find documents: ${err}`))

});

module.exports = router;
/*

const query = { "reviews.0": { "$exists": true } };
const projection = { "_id": 0 };

return itemsCollection.find(query, projection)
  .sort({ name: 1 })
  .toArray()
  .then(items => {
    console.log(`Successfully found ${items.length} documents.`)
    items.forEach(console.log)
    return items
  })
  .catch(err => console.error(`Failed to find documents: ${err}`))

*/
