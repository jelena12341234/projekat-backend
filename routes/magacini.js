const express = require('express');
const router = express.Router();




const config = require('../config/database');



const Magacin = require('../models/magacin');


router.post('/dodajProizvod', (req, res, next) => {

    const ras = [{
        nazivRasadnika: req.body.nazivRasadnika,
        username: req.body.username,
        tip: req.body.tip,
        naziv: req.body.naziv,
        proizvodjac: req.body.proizvodjac,
        kolicina: req.body.kolicina,
        arrived: req.body.arrived
    }];

    Magacin.insertMany(ras, err => {
        if (err) res.json({ success: false, msg: "error" });
        else res.json({ success: true, msg: "success" });
    });

});

router.post('/changeQuantity',(req,res,next)=>{
    const name = req.body.imeSadnice;
    const korIme= req.body.username;
    const kol = req.body.kolicina;
    const nazivrs = req.body.nazivRasadnika;

    console.log("/changeQuantity"+name+","+korIme+","+kol+","+nazivrs);
    
    Magacin.changeQuantity(nazivrs, korIme,name, kol,err=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json({success:true, msg:"success"});  
    });
});


router.post('/magacinInfo', (req, res, next) => {

    const korIme = req.body.username;
    const nazivrs = req.body.nazivRasadnika;

    //showSad = function(korIme, nazivrs, callback)
    Magacin.showMagacine(korIme, nazivrs, (err, data) => {
        if (err) res.json({ success: false, msg: "error" });
        else res.json(data);
    });


});

router.post('/sortirajponazivu', (req, res, next) => {

    const usr = req.body.username;
    const nazivrs = req.body.nazivRasadnika;


    Magacin.find({ username: usr, nazivRasadnika: nazivrs }).sort({ "naziv": 1 }).then(data => {

        res.json(data);

    }).catch(err => console.error(`Failed to find documents: ${err}`));

});


router.post('/sortirajpokolicini', (req, res, next) => {
    const usr = req.body.username;
    const nazivrs = req.body.nazivRasadnika;

    Magacin.find({ username: usr, nazivRasadnika: nazivrs }).sort({ "kolicina": 1 }).then(data => {

        res.json(data);

    }).catch(err => console.error(`Failed to find documents: ${err}`));

});


router.post('/sortirajpoproizvodjacu', (req, res, next) => {
    const usr = req.body.username;
    const nazivrs = req.body.nazivRasadnika;

    Magacin.find({ username: usr, nazivRasadnika: nazivrs }).sort({ "proizvodjac": 1 }).then(data => {

        res.json(data);

    }).catch(err => console.error(`Failed to find documents: ${err}`));

});

router.post('/filter', (req, res, next) => {
    const usr = req.body.username;
    const nazivrs = req.body.nazivRasadnika;
    const kol = req.body.kolicina;
    const name = req.body.naziv;
    const proiz = req.body.proizvodjac;

    //ako su sva tri razlicita
    if (name != "..po nazivu" && proiz != "..po proizvođaču" && kol != "..po količini") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, kolicina: kol, naziv: name, proizvodjac: proiz }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (name != "..po nazivu" && proiz != "..po proizvođaču") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, naziv: name, proizvodjac: proiz }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (proiz != "..po proizvođaču" && kol != "..po količini") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, kolicina: kol, proizvodjac: proiz }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (name != "..po nazivu" && kol != "..po količini") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, kolicina: kol, naziv: name }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (name != "..po nazivu") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, naziv: name }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (kol != "..po količini") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, kolicina: kol }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    } else if (proiz != "..po proizvođaču") {
        Magacin.find({ username: usr, nazivRasadnika: nazivrs, proizvodjac: proiz }).then(data => {
            res.json(data);
        }).catch(err => console.error(`Failed to find documents: ${err}`));
    }


});

module.exports = router;
