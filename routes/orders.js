const express = require('express');
const router = express.Router();



const config = require('../config/database')

const Order = require('../models/order');

router.post('/addOrder',(req,res,next)=>{

    const ras=[{
     proizvodjac : req.body.proizvodjac,
     tip : req.body.tip,
     naziv : req.body.naziv,
     kolicina : req.body.kolicina,
     username: req.body.username,
     datum: req.body.datum
    }]

    Order.insertMany(ras,err=>{
        if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});        
    });
});


router.post('/getOrders',(req,res,next)=>{
    const proiz = req.body.username;
    Order.find({proizvodjac:proiz}).then(data=>{
        res.json(data);
       }).catch(err => console.error(`Failed to find documents: ${err}`));  
});


router.post('/sort',(req,res,next)=>{
    const usr = req.body.username;

    Order.find({proizvodjac:usr}).sort({"datum":1}).then(data=>{

    res.json(data);

   }).catch(err => console.error(`Failed to find documents: ${err}`));  
    
});

router.post('/decline',(req,res,next)=>{


    const username = req.body.username;
    const proizvodjac = req.body.proizvodjac;
    const naziv = req.body.naziv;
    const datum = req.body.datum;

    
    Order.deny(username,proizvodjac,naziv,datum,err=>{
        if(err) res.json({success: false, msg: "Order not denied"});
    });
});




module.exports = router;
