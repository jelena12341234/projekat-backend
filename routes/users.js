const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const config = require('../config/database')

const User = require('../models/user');

router.post('/register', (req,res,next)=>{
    
    let newUser = new User({
        ime: req.body.ime,
        prezime: req.body.prezime,
        naziv:req.body.naziv,
        username: req.body.username,
        password : req.body.password,
        date: req.body.date, 
        place: req.body.place,
        phone: req.body.phone,
        mail: req.body.mail,
        approved: req.body.approved,
        typeOfUser: req.body.typeOfUser
    });
  
    User.addUser(newUser, (err,user)=>{
        if(err){
            res.json({success:false, msg:'Failed to register user'});
        } else{
            res.json({success:true, msg:'User registered'});    
        }
    });


});



router.get('/registerRequests', (req,res,next)=>{
    User.showUsers((err,data)=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json(data);
    });

});


router.get('/registerPreduzeca',(req,res,next)=>{
    User.showPreduzeca((err,data)=>{
        if(err) res.json({success: false, msg: "error"});
        else res.json(data);
    });
});


router.post('/denyUser',(req,res,next)=>{

    const username = req.body.username;

    console.log("usli smo u /denyUser sa imenom "+username);

    User.deny(username,err=>{
        if(err) res.json({success: false, msg: "error"});
    });

});

router.post('/acceptUser',(req,res,next)=>{
    
    const username = req.body.username;
    console.log("usli smo u /acceptUser sa korimenom "+ username );   
    User.accept(username,err=>{
        if(err) res.json({success: false, msg: "error"});
    });

});



router.post('/check',(req,res,next)=>{

    const username = req.body.username;
    User.getUserByUsername(username,( err,user)=>{
        if(err){ res.json({success: false, msg: `Database error: ${err}`});}
        if(!user){
            console.log("User not found with name" + username);
            return res.json({success:false, msg:'User not found'});
        }  else{   
            console.log("User found with name" + username);
            return  res.json({success:true, msg:'User found'});
        }
});
});

router.post('/getUser',(req,res,next)=>{
    const username = req.body.username;
    User.getUserByUsername(username,( err,user)=>{
        if(err){res.json({success: false, msg: `Database error: ${err}`});}
        else return res.json(user);
    });
});


router.post('/authenticate', (req,res,next)=>{

    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username,( err,user)=>{
        if(err) throw err;
        if(!user){
            return res.json({success:false, msg:'User not found'});
        } 


        User.comparePassword(password,user.password, (err, isMatch)=>{
            if(err) throw err;
            if(isMatch){
                const token = jwt.sign({_id: user._id}, config.secret,{
                    expiresIn:3600 //1 week
                });

                res.json({
                    success:true,
                    token: 'JWT' + token,
                    user:{
                        id: user._id,
                        ime: user.ime,
                        prezime: user.prezime,    
                        username: user.usename,
                        date: user.date,
                        place:user.place,
                        phone:user.phone,
                        mail: user.mail,
                        approved: user.approved,
                        typeOfUser: user.typeOfUser
                    }
                });
            } else{
                return res.json({success:false, msg:'Wrong password'});
            }
        });
    });
});


router.get('/profile', (req,res,next)=>{
    res.send('PROFILE');
});



module.exports = router;