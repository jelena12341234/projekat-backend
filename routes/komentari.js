const express = require('express');
const router = express.Router();



const config = require('../config/database')

const Komentar = require('../models/komentar');

/**
 *   proizvodjac: {
        type:String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    username:{                  //ova tri polja predstavljaju kljuc
        type:String,
        required:true
    },
    komentar:{
        type:String,
        required:true
    },
    ocena:{
        type:Number,
        required:true
 */

router.post('/putComm',(req,res,next)=>{

    const comm= [{
        ocena:req.body.ocena,
        komentar:req.body.komentar,
        username:req.body.username,
        naziv:req.body.naziv,
        proizvodjac:req.body.proizvodjac
    }]

    
    Komentar.insertMany(comm,err=>{
        if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});        
    });

});


router.post('/getComms',(req,res,next)=>{
    proizv=req.body.proizvodjac;
    name  = req.body.naziv;

    Komentar.find({proizvodjac:proizv, naziv:name}).then(data=>{
        res.json(data);
       }).catch(err => console.error(`Failed to find documents: ${err}`));  
});




module.exports = router;
