const express = require('express');
const router = express.Router();



const config = require('../config/database')

const Proizvod = require('../models/proizvod');


router.post('/addProizvod',(req,res,next)=>{

    const ras=[{
     proizvodjac : req.body.proizvodjac,
     tip : req.body.tip,
     naziv : req.body.naziv,
     kolicina : req.body.kolicina,
     available : req.body.available,
     prosecnaOcena: req.body.prosecnaOcena
    }]

    console.log(ras);

    Proizvod.insertMany(ras,err=>{
        if(err) res.json({success: false, msg: "error"});
            else res.json({success:true, msg:"success"});        
    });

});


router.get('/getProizvodi',(req,res,next)=>{
    Proizvod.find({}).then(data=>{
        res.json(data);
       }).catch(err => console.error(`Failed to find documents: ${err}`));  
});

router.post('/getMyProducts',(req,res,next)=>{
    const proizv = req.body.username;
    Proizvod.find({"proizvodjac":proizv}).then(data=>{
        res.json(data);
       }).catch(err => console.error(`Failed to find documents: ${err}`));  
});


module.exports = router;
