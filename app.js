//pocetna tacka servera
const express=require('express');
const path = require('path');
const bodyParser = require('body-parser')
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
var nodemailer = require('nodemailer');


//connect to database
mongoose.connect(config.database);


//on connection
mongoose.connection.on('connected', ()=>{
    console.log('connected to database ' + config.database);
});


mongoose.set('useFindAndModify', false);

mongoose.connection.on('error', (err)=>{
    console.log('Database error ' + err);
});


const app = express();

const port = 3000;
const users = require('./routes/users');
const rasadnici = require('./routes/rasadnici');
const sadnice = require('./routes/sadnice');
const magacini = require('./routes/magacini');
const proizvodi = require('./routes/proizvodi');
const komentari = require('./routes/komentari');
const orders = require('./routes/orders');

//CORS middleware
app.use(cors());

//body parser middleware
app.use(bodyParser.json());

//passport malware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);


//database
app.use('/users',users);
app.use('/rasadnici',rasadnici);
app.use('/sadnice',sadnice);
app.use('/magacini',magacini);
app.use('/proizvodi',proizvodi);
app.use('/komentari',komentari);
app.use('/orders',orders);

//set static folder
//starter page
app.use(express.static(path.join(__dirname,'public')));

//index route
app.get('/', (req, res)=>{
    res.send('Invalid endpoint');
});

//Start server
app.listen(port, ()=>{
    console.log('Server se aktivirao na portu ' + port);
});




const Sadnica = require('./models/sadnica');
const User = require('./models/user');
const Rasadnik = require('./models/rasadnik');



//ista fja treba i za temp i vodu
setInterval(function(){
   
    Sadnica.find().then(
        items=>{
            items.forEach(item=>{
                var myquery = { "username" : item.username, "nazivRasadnika" :item.nazivRasadnika, "mesto":item.mesto, "naziv":item.naziv};
                   var prog = item.progress + 1;
                   if(prog<=100){
                   var newvalues = { "$set": {"progress" : prog}};

                   Sadnica.updateMany(myquery,newvalues, (err)=>{
                    if(err)     console.log(err);
                   });
                }
            });

       //     console.log(items);
        }
    ).catch(err => console.error(`Failed to find documents: ${err}`));  
  
}, 10000);

/*


var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'jradojkovic22@gmail.com',
      pass: 'likeag6xoxo'
    }
  });


  
  setInterval(function(){
    var email='';

    //{ $or: [ { quantity: { $lt: 20 } }, { price: 10 } ] } 
    Rasadnik.find({ $or:[  {temperatura: {$lt:10}}, {kolVode: {$lt:75}} ]}).then(
        items=>{
            items.forEach(item=>{
                User.find({username:item.username}).then(
                    users=>{

                        users.forEach(user=>{
                        email=user.mail;

                        var mailOptions = {
                            from: 'jradojkovic22@gmail.com',
                            to: email,
                            subject: 'Sending Email using Node.js',
                            text: 'Vašem rasadniku je potrebno održavanje!'
                          };
                        
                            transporter.sendMail(mailOptions, function(error, info){
                                if (error) {
                                  console.log(error);
                                } else {
                                  console.log('Email sent: ' + info.response);
                                }
                              });

                        });
                    }
                );
            });
        }
    ).catch(err => console.error(`Failed to find documents: ${err}`));  
  },10000);

  */