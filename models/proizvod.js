const mongoose = require('mongoose');



const ProizvodSchema = mongoose.Schema({

    proizvodjac: {
        type:String,
        required:true   
    },
    tip:{                   //da li je preparat ili sadnica
        type: String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    kolicina:{
        type: Number,
        required:true
    },
    available:{
        type:Boolean,
        required:true
    },
    prosecnaOcena:{
        type:Number,
        required:true
    }
});




const Proizvod = module.exports = mongoose.model('Proizvod', ProizvodSchema);

/*

module.exports.showMagacine = function(korIme, nazivrs, callback){     
    Magacin.find({username:korIme, nazivRasadnika:nazivrs},callback);   
}
*/
