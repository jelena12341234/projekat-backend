const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');



const UserSchema = mongoose.Schema({

    ime:{
        type: String,
    },
     prezime:{
        type: String,   
    },
    naziv: {
        type:String
    },
    username:{              //skraceni naziv preduzeca
        type: String,
        required:true   
    },
    password:{
        type: String,
        required:true   
    },
    date:{
        type: String,
        required:true   
    },
    place:{
        type: String,
        required:true   
    },
    phone:{
        type: String,
    },
    mail:{
        type: String,
        required:true   
    },
    approved: {
        type:Boolean,
        required:true
    },
    typeOfUser: {
        type: String,
        required:true
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id,callback){
    User.findById(id,callback);
}

module.exports.getUserByUsername = function(username,callback){
    const query = {username:username}
    User.findOne(query,callback);
}

module.exports.showUsers = function(callback){
    User.find({approved:false,typeOfUser:"poljoprivrednik"},callback);   
}
//typeOfUser:"poljoprivrednik"
module.exports.showPreduzeca = function(callback){
    User.find({approved:false,typeOfUser:"preduzece"},callback);
}


module.exports.accept = function(username, callback){
    console.log("u accept fji za bazu je stigao zahtev za korImenom: "+ username);
    var myquery = { "username": username};
    var newvalues = { "$set": {"approved": true}};
    User.updateOne(myquery,newvalues,callback);
}


//if denied then deleted
module.exports.deny = function(username, callback){
    
    console.log("u deny fji za bazu je stigao zahtev za korImenom: "+ username);
//    var myquery = { username: username}
    User.deleteOne({"username":username},callback);
}



module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10, (err,salt)=>{
        bcrypt.hash(newUser.password, salt, (err,hash)=>{
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        })
    });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash,(err, isMatch)=>{
      if(err) throw err;
      callback(null,isMatch);
  });  
}