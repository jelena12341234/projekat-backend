const mongoose = require('mongoose');



const OrderSchema = mongoose.Schema({

    proizvodjac: {
        type:String,
        required:true   
    },
    tip:{                   //da li je preparat ili sadnica
        type: String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    kolicina:{
        type: Number,
        required:true
    },
    username:{
        type:String,
        required:true
    },
    datum:{
        type:String,
        required:true
    }
});




const Order = module.exports = mongoose.model('Order', OrderSchema);


module.exports.deny = function(username,proizvodjac,naziv,datum, callback){
    
   Order.deleteOne({"username":username, "proizvodjac":proizvodjac,"naziv":naziv,"datum":datum},callback);
}
