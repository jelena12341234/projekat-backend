const mongoose = require('mongoose');



const KomentarSchema = mongoose.Schema({

    proizvodjac: {
        type:String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    username:{                  //ova tri polja predstavljaju kljuc
        type:String,
        required:true
    },
    komentar:{
        type:String,
        required:true
    },
    ocena:{
        type:Number,
        required:true
    }
});




const Komentar = module.exports = mongoose.model('Komentar', KomentarSchema);

