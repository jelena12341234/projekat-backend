const mongoose = require('mongoose');



const MagacinSchema = mongoose.Schema({

    nazivRasadnika: {
        type:String,
        required:true   
    },
    username:{              //username poljoprivrednika kome pripada rasadnik
        type: String,
        required:true   
    },
    tip:{                   //da li je preparat ili sadnica
        type: String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    proizvodjac:{
        type: String,
        required:true   
    },
    kolicina:{
        type: Number,
        required:true
    },
    arrived:{
        type:Boolean,
        required:true
    }
});




const Magacin = module.exports = mongoose.model('Magacin', MagacinSchema);


module.exports.changeQuantity = function (nazivrs, korIme,name, kol, callback) {
    console.log("magacin i"+nazivrs+","+korIme+","+name+","+kol);
    Magacin.findOneAndUpdate({ "nazivRasadnika": nazivrs, "username":korIme, "naziv":name }, { "kolicina": kol }, callback);
}


module.exports.showMagacine = function(korIme, nazivrs, callback){     
    Magacin.find({username:korIme, nazivRasadnika:nazivrs},callback);   
}



//module.exports.arrive = function (name, temp, callback) {
 //   Rasadnik.findOneAndUpdate({ "naziv": name }, { "arrived": true }, callback);
//}