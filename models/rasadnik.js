const mongoose = require('mongoose');
const config = require('../config/database');


const RasadnikSchema = mongoose.Schema({
    naziv: {
        type: String,
        required: true
    },
    username: {              //username poljoprivrednika kome pripada rasadnik
        type: String,
        required: true
    },
    mesto: {
        type: String,
        required: true
    },
    brojSadnica: {
        type: Number,
        required: true
    },
    brojSlMesta: {
        type: Number,
        required: true
    },
    kolVode: {
        type: Number,
        required: true
    },
    temperatura: {
        type: Number,
        required: true
    }, 
    sirina : {
        type:Number
    },
    visina : {
        type:Number
    }
});

const Rasadnik = module.exports = mongoose.model('Rasadnik', RasadnikSchema);


module.exports.showRas = function (korIme, callback) {        //vazi za sve rasadnike
    Rasadnik.find({ username: korIme }, callback);
}

module.exports.prikaziRasadnik = function (korime, name, place, callback) {
    Rasadnik.find({ username: korime, naziv: name, mesto: place }, callback);
}


module.exports.changeTemp = function (name, temp, callback) {
    Rasadnik.findOneAndUpdate({ "naziv": name }, { "temperatura": temp }, callback);
}

module.exports.changeWater = function (name, water, callback) {
    Rasadnik.findOneAndUpdate({ "naziv": name }, { "kolVode": water }, callback);
}

module.exports.changeNumSadnica = function (name, br1,br2, callback) {
    Rasadnik.findOneAndUpdate({ "naziv": name }, { "brojSadnica": br1, "brojSlMesta":br2 }, callback);
}