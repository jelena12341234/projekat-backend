const mongoose = require('mongoose');


const SadnicaSchema = mongoose.Schema({

    nazivRasadnika: {
        type:String,
        required:true   
    },
    username:{              //username poljoprivrednika kome pripada rasadnik
        type: String,
        required:true   
    },
    mesto:{
        type: String,
        required:true   
    },
    naziv:{
        type: String,
        required:true   
    },
    proizvodjac:{
        type: String,
        required:true   
    },
    progress:{
        type: Number,
        required:true
    },
    row:{
        type: Number,
        required:true    
    },
    col:{
        type: Number,
        required:true
    },
    izvadjena:{
        type:Boolean
    }

});




const Sadnica = module.exports = mongoose.model('Sadnica', SadnicaSchema);


module.exports.showSad = function(korIme, place, nazivrs, red, callback){        //vazi za sve rasadnike
    Sadnica.find({username:korIme, mesto: place, row:red, nazivRasadnika:nazivrs},callback);   
}



module.exports.showSadInfo = function(korIme, nzv, proiz, place, nazivrs,red,kol, callback){        //vazi za sve rasadnike
    Sadnica.find({username:korIme, naziv:nzv, proizvodjac: proiz,row:red, col:kol, mesto: place, nazivRasadnika:nazivrs},callback);   
}


module.exports.izvadi = function (nazivrs, korIme,name, kol,red,place, callback) {
    console.log("izvadi"+nazivrs+","+korIme+","+name+","+kol);
   Sadnica.findOneAndUpdate({ "nazivRasadnika": nazivrs, "mesto":place,"username":korIme, "naziv":name, "col":kol, "row":red }, { "izvadjena": true }, callback);
}

module.exports.obrisi = function (nazivrs, korIme,name, kol,red,place, callback) {
    console.log("obrisi"+nazivrs+","+korIme+","+name+","+kol);
   Sadnica.remove({ "nazivRasadnika": nazivrs, "mesto":place,"username":korIme, "naziv":name, "col":kol, "row":red }, callback);
}
